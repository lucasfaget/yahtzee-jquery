class Dice
{
    value
    isLocked = false

    constructor(value) {
        if (value === undefined) {
            value = Dice.randomDiceValue()
        }
        this.value = value
    }

    /**
     * Return an array of all possible dice value
     * @returns Array
     */
    static values() {
        return [1,2,3,4,5,6]
    }

    /**
     * Return a random number between 1 and max
     * @param {number} max 
     * @returns number
     */
    static randomDiceValue() {
        return Math.floor(1 + Math.random() * Dice.values().length)
    }
}

class Round
{
    diceDeck = []
    maxRollNumber = 2 // Maximum number of roll
    rollNumber = 0 // Current number of roll
    isScoreLocked = false

    throw() {
        if (this.rollNumber === 0) {
            for (let i=0 ; i<5 ; i++) {
                this.diceDeck.push(new Dice())
            }
        } else {
            for (const dice of this.diceDeck) {
                if (!dice.isLocked) {
                    dice.value = Dice.randomDiceValue()
                    dice.isLocked = false
                }
            }
        }
        this.rollNumber++
    }

    isOver() {
        return this.rollNumber === this.maxRollNumber
    }
}

class Yahtzee
{
    round = new Round()
    scores = new Scores()

    play() {
        if (!this.round.isOver()) {
            this.round.throw()
            this.scores.updateScores(this.round.diceDeck)
            this.updateBoard()
        }     
    }

    updateBoard() {
        $(".board-card-rolled-dice").empty()
        if (!this.round.isOver()) {
            this.round.diceDeck.forEach((dice, index) => {
                if (!dice.isLocked) {
                    $(".board-card-rolled-dice").append(
                        `<img class="board-dice" src="./assets/dice/${dice.value}.svg" data-index-number="${index}" />`
                    )
                }
            })
        } else {
            $(".board-card-locked-dice").empty()
            this.round.diceDeck.forEach((dice) => {
                $(".board-card-final-dice").append(
                    `<img class="board-dice" src="./assets/dice/${dice.value}.svg" />`
                )
            })
        }
        console.log("board updated")
    }

    updateTable() {
        console.log("table updated")
    }

    lockScore(id) {
        if (this.round.isOver() && !this.round.isScoreLocked) {
            if (id === "bonus") {
                this.scores.bonus.isLocled = true;
            } else {
                if (this.scores.upperSection[id]) {
                    this.scores.upperSection[id].isLocked = true
                } else {
                    console.log('lock lower section')
                    this.scores.lowerSection[id].isLocked = true
                }
            }
            $(`#${id}`).addClass("locked-box")
            this.round.isScoreLocked = true
            this.scores.updateTotals()
            $(".board-card-final-dice").empty()
            this.round = new Round()
            console.log("new round !")
        }
    }
}

class Scores
{
    upperSection = {
        ones: {value: 1, score: 0, isLocked: false},
        twos: {value: 2, score: 0, isLocked: false},
        threes: {value: 3, score: 0, isLocked: false},
        fours: {value: 4, score: 0, isLocked: false},
        fives: {value: 5, score: 0, isLocked: false},
        sixes: {value: 6, score: 0, isLocked: false},
    }

    lowerSection = {
        threeOfAKind: {score: 0, isLocked: false},
        fourOfAKind: {score: 0, isLocked: false},
        fullHouse: {score: 0, isLocked: false},
        smStraight: {score: 0, isLocked: false},
        lgStraight: {score: 0, isLocked: false},
        yahtzee: {score: 0, isLocked: false},
        chance: {score: 0, isLocked: false},
        yahtzeeBonus: {score: 0, isLocked: false},
    }

    updateScores(diceDeck) {
        // for each dice value, count the dice number
        let diceNumberByValue = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0 }
        for (const dice of diceDeck) {
            diceNumberByValue[dice.value]++
        }

        // ==================================
        // ----- fill the upper section -----
        // ==================================
        for (const [id, data] of Object.entries(this.upperSection)) {
            if (!data.isLocked) {
                data.score = diceNumberByValue[data.value] * data.value
                $(`#${id}`).text(data.score)
            }
        }
        
        // ==================================
        // ----- fill the lower section -----
        // ==================================
        if (!this.lowerSection.threeOfAKind.isLocked) {
            this.lowerSection.threeOfAKind.score = 0
            for (const [value, number] of Object.entries(diceNumberByValue)) {
                if (number >= 3) {
                    if (number * value > this.lowerSection.threeOfAKind.score) {
                        this.lowerSection.threeOfAKind.score = 3 * value
                        if (!this.lowerSection.fourOfAKind.isLocked) {
                            this.lowerSection.fourOfAKind.score = 0
                            if (number >= 4) {
                                if (number * value > this.lowerSection.threeOfAKind.score) {
                                    this.lowerSection.fourOfAKind.score = 4 * value
                                    if (!this.lowerSection.yahtzee.isLocked) {
                                        this.lowerSection.yahtzee.score = 0
                                        if (number == 5) {
                                            this.lowerSection.yahtzee.score = 50
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        this.lowerSection.chance.score = Object.values(this.lowerSection).reduce(
            (sum, currentData) => sum + currentData.score, 0
        )
        
        for (const [id, data] of Object.entries(this.lowerSection)) {
            if (!data.isLocked) {
                $(`#${id}`).text(data.score)
            }
        }
    }

    updateTotals() {
        let upperSectionTotal = Object.values(this.upperSection).reduce(
            (sum, currentData) => sum + (currentData.isLocked ? currentData.score : 0), 0
        )
        let lowerSectionTotal = Object.values(this.lowerSection).reduce(
            (sum, currentData) => sum + (currentData.isLocked ? currentData.score : 0), 0
        )

        if (upperSectionTotal >= 20) {
            upperSectionTotal += 35
            $('#bonus').text(35)

        }

        $('#upperSectionTotal').text(upperSectionTotal)
        $('#lowerSectionTotal').text(lowerSectionTotal)

        $('#total').text(
            upperSectionTotal + lowerSectionTotal
        )
    }
}

let yahtzee = new Yahtzee()

// Hover a dice animation
// $(".board-card-rolled-dice .board-dice").mouseenter(function() {
//     $(this).stop() // cancel the previous animation
//     $(this).animate({
//         width: "60px"
//     }, 200);
// });
// $(".board-card-rolled-dice .board-dice").mouseleave(function() {
//     $(this).stop() // cancel the previous animation
//     $(this).animate({
//         width: "50px"
//     }, 0);
// });

// Push the button "Roll" animations
$(".roll-button").mousedown(function() {
    yahtzee.play()
    $(this).stop() // cancel the previous animation
    $(this).animate({
        width: "80px"
    }, 100)
});
$(".roll-button").mouseup(function() {
    $(this).stop() // cancel the previous animation
    $(this).animate({
        width: "100px"
    }, 100)
});

// Click on a dice to lock it
$(".board-card-rolled-dice").on("click", ".board-dice", function() {
    $(this).stop() // cancel the previous animation
    let index = $(this).data("index-number")
    yahtzee.round.diceDeck[index].isLocked = true
    $(this).appendTo(".board-card-locked-dice")
});

// Click on a points box to lock it
$(".points-box").click(function() {
    let id = $(this).attr('id')
    yahtzee.lockScore(id)
});

